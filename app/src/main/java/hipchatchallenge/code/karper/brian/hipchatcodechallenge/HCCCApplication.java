package hipchatchallenge.code.karper.brian.hipchatcodechallenge;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by briank on 4/29/2015.
 */
public class HCCCApplication extends Application {

    private static HCCCApplication Instance;

    public static HCCCApplication getInstance() {
        return Instance;
    }

    public static Context getContext() {
        return Instance.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Instance = this;
    }
}
