package hipchatchallenge.code.karper.brian.hipchatcodechallenge.asynctasks;

import android.os.AsyncTask;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by briank on 4/20/2015.
 */
public class TitleParserAsynctask extends AsyncTask {

    private String titleRegex = "<title>(.*?)</title>";
    private Pattern titlePattern = Pattern.compile(titleRegex, 0);
    private Matcher parseMatcher;

    @Override
    protected Object doInBackground(Object[] params) {
        StringBuilder str = new StringBuilder();
        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet request = new HttpGet(new URI(params[0].toString().trim()));
            HttpResponse response = client.execute(request);
            InputStream in = response.getEntity().getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String line = null;
            while((line = reader.readLine()) != null) {
                parseMatcher = titlePattern.matcher(line);
                while (parseMatcher.find()) {
                    str.append(parseMatcher.group().substring(7, parseMatcher.group().length()-8));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return str.toString();
    }


}
