package hipchatchallenge.code.karper.brian.hipchatcodechallenge.Fragment;

import android.app.Fragment;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import hipchatchallenge.code.karper.brian.hipchatcodechallenge.R;
import hipchatchallenge.code.karper.brian.hipchatcodechallenge.utils.ParseUtils;

/**
 * Created by briank on 4/29/2015.
 */
public class ChatMessageFragment extends Fragment {

    private View v;
    private EditText etMessage;
    private Button btnParse;

    public static ChatMessageFragment NewInstance(Bundle args) {
        ChatMessageFragment fragment = ChatMessageFragment.NewInstance();
        fragment.setArguments(args);
        return fragment;
    }

    public static ChatMessageFragment NewInstance() {
        return new ChatMessageFragment();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.chat_message_fragment, null);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        init();
    }

    private void init() {
        etMessage = (EditText) v.findViewById(R.id.etMessage);
        btnParse = (Button) v.findViewById(R.id.btnParse);
        btnParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Send Message To Server Asynctask
                if (!etMessage.getText().toString().isEmpty()) {
                    String json = ParseUtils.ParseMessage(etMessage.getText());
                    Log.d("JSON OUTPUT", json);
                    ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                    ClipData clip = ClipData.newPlainText("JSON", json);
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getActivity(), "JSON Copied to Clipboard", Toast.LENGTH_SHORT).show();
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://jsonlint.com/"));
                    startActivity(browserIntent);
                }
            }
        });
    }
}
