package hipchatchallenge.code.karper.brian.hipchatcodechallenge.activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import hipchatchallenge.code.karper.brian.hipchatcodechallenge.Fragment.ChatMessageFragment;
import hipchatchallenge.code.karper.brian.hipchatcodechallenge.R;


public class MainActivity extends ActionBarActivity {

    private FragmentManager fm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        init();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void init() {
        fm = getFragmentManager();
        FragmentTransaction message = fm.beginTransaction();
        message.add(R.id.frmChatMessage, ChatMessageFragment.NewInstance());
        message.commit();
    }
}
