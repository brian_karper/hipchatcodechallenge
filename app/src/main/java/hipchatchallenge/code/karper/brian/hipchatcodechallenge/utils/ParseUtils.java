package hipchatchallenge.code.karper.brian.hipchatcodechallenge.utils;

import android.text.Editable;

import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hipchatchallenge.code.karper.brian.hipchatcodechallenge.asynctasks.TitleParserAsynctask;

/**
 * Created by briank on 4/20/2015.
 */
public class ParseUtils {

    private static StringBuilder jsonBuilder;

    public static String ParseMessage(Editable message) {
        jsonBuilder = new StringBuilder();
        jsonBuilder.append("{ \"message\" : { \"text\": \"" + message + "\",");
        ParseMentions(message);
        jsonBuilder.append(",");
        ParseEmoticons(message);
        jsonBuilder.append(",");
        ParseLinks(message);
        jsonBuilder.append("}}");
        return jsonBuilder.toString();
    }

    public static void ParseMentions(Editable message) {
        String mentionsRegex = "(@\\w+)";
        Pattern mentionsPattern = Pattern.compile(mentionsRegex, 0);
        Matcher parseMatcher = mentionsPattern.matcher(message);
        jsonBuilder.append("\"mentions\": [ ");

        while (parseMatcher.find()) {
            jsonBuilder.append("\"" + parseMatcher.group().substring(1) + "\"");
            jsonBuilder.append(",");
        }
        jsonBuilder.replace(jsonBuilder.length() - 1, jsonBuilder.length(), "]");
    }

    public static void ParseEmoticons(Editable message) {
        String emoticonsRegex = "(\\([A-Za-z]{0,15}\\))";
        Pattern emoticonsPattern = Pattern.compile(emoticonsRegex, 0);
        Matcher parseMatcher = emoticonsPattern.matcher(message);
        jsonBuilder.append("\"emoticons\": [ ");

        while (parseMatcher.find()) {
            jsonBuilder.append("\"" + parseMatcher.group().substring(1, parseMatcher.group().length() -1) + "\"");
            jsonBuilder.append(",");
        }
        jsonBuilder.replace(jsonBuilder.length() - 1, jsonBuilder.length(), "]");
    }

    public static void ParseLinks(Editable message) {
        String emoticonsRegex = "((http://|https://)(www.)?([a-zA-Z0-9]+).[a-zA-Z0-9]*.[a-z]{3}.?)";
        Pattern emoticonsPattern = Pattern.compile(emoticonsRegex, 0);
        Matcher parseMatcher = emoticonsPattern.matcher(message);
        jsonBuilder.append("\"links\": [ ");

        while (parseMatcher.find()) {
            jsonBuilder.append("{ \"url\":");
            jsonBuilder.append("\"" + parseMatcher.group().trim() + "\"");
            jsonBuilder.append(",");
            jsonBuilder.append("\"title\":");
            try {
            jsonBuilder.append("\"" + new TitleParserAsynctask().execute(parseMatcher.group()).get().toString().trim() +"\"");
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            jsonBuilder.append("},");
        }
        jsonBuilder.replace(jsonBuilder.length() - 1, jsonBuilder.length(), "]");
    }
}
